# nodejs-sql-boilerplate

basic setup for node-express-sql stack

# Auto generate migration files

`npm run db:makemigrations`

Refer to this [stackoverflow answer](https://stackoverflow.com/a/59021807/6911537)

# Migrate
`npx sequelize db:migrate`